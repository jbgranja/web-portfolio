import React, { Component } from 'react';
import { useTranslation } from "react-i18next";

import About from './Pages/About';
import My_work from './Pages/My_work';
import Home from './Pages/Home';

import styled from 'styled-components';

import { BrowserRouter as Router, Route, Link } from "react-router-dom";

const Navmenu = styled.nav` 
    padding:20px 10%;
    text-align:right;

    @media (max-width:768px){
        padding:20px 10px;
    }
    `;


const HomeLink = styled(Link)`
color:#000;
text-transform:uppercase;
margin-left:10px;
font-weight:lighter;
&:first-child{
    float:left;

    border: 2px solid #000000;
    padding: 9px 50px;
}
&:hover{
    font-weight:bold;
    text-decoration:underline;
}
`

const NavLink = styled(Link)`   
    color:#000;
    text-transform:uppercase;
    margin-left:10px;
    font-weight:lighter;
    
`;

const Pages = styled.div` 
    padding:20px 10%;

    @media (max-width:768px){
        padding:20px 10px;
    }
`;

function Navigator() {

    const { t, i18n } = useTranslation();

    const changeLanguageHandler = (e) => {
        const languageValue = e.target.value
        i18n.changeLanguage(languageValue);
    }
    return (
        <Router>
            <div>

                <Navmenu className="Navigator">

                    <HomeLink to="/">Home</HomeLink>
                    <select className="custom-select" style={{ width: 200 }} onChange={changeLanguageHandler}>
                        <option value="en" >{t('languaje_link_english')}</option>
                        <option value="es" >{t('languaje_link_spanish')}</option>
                    </select>

                    <div className='navLinks'>
                    <NavLink to='/portfolio'>{t('portfolio_link')}</NavLink>
                    <NavLink to='/about'>{t('about_link')}</NavLink>
                    <a class="NavLink" rel="noopener noreferrer" target="_blank" href='https://stellular-snickerdoodle-2c660d.netlify.app/'>Blog</a>
                    </div>
                    
                </Navmenu>
                <Pages>
                    <Route path="/about" component={About} />
                    <Route path="/portfolio" component={My_work} />

                    <Route exact path="/" component={Home} />
                </Pages>
            </div>
        </Router>

    );

}

export default Navigator;
