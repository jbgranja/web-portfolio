import React from 'react';
import styled from 'styled-components';
import { useTranslation } from "react-i18next";
import Form from '../Components/Form';

const ContainerDiv = styled.div`
  padding: 0 ;
`;

const ProfilePic = styled.img`
  max-width:200px;
  display:block;
  margin:10px 0;
`;


function About() {

  const { t } = useTranslation();

  return (


    <ContainerDiv className="About">


      <div>

        <h1>Juan Granja </h1>


        <ProfilePic src="https://bitbucket.org/jbgranja/web-portfolio/raw/9dde5a5fb34996762c0512c1d1d053688ede2770/public/assets/profile_pic.jpg" alt="Profile Image" />


      </div>

      <div>

        <span>Full Stack Developer</span>
        <p>
          {t('portfolio_copy_1')}
        </p>
        <p>
          {t('portfolio_copy_2')}
        </p>
        <h3>

          {t('aboutme_download_cv_copy')}
        </h3>
      </div>



      <div>
        <a target="_blank" rel="noopener noreferrer" href="https://drive.google.com/file/d/1hgEhXHJF6gdk0DeCE5_oXQXJ4O3vH-M8/view?usp=sharing" download><i className="far fa-file"></i> </a>
        <br />
        <a class="email_btn" href="mailto:ggjuanb@hotmail.com">{t('aboutme_email_contact')}</a> :

        <br />
        <br /> <a href="mailto:ggjuanb@hotmail.com">ggjuanb@hotmail.com</a>
        <Form />

      </div>

    </ContainerDiv>
  )

}



export default About;
