import React from 'react';
import ServicesContainer from '../Components/ServicesContainer';
import SkillsContainer from '../Components/SkillsContainer';
import styled from 'styled-components';
import { useTranslation } from "react-i18next";

const InformativeDiv = styled.nav` 
    width:50%;
    display:inline-block;
    text-align:left;
    vertical-align:middle;
    padding:10px;
    box-sizing:border-box;
    @media (max-width: 768px) {
      flex-direction: column;
      width:100%;
    }`;


const Title = styled.h2`
      text-align:center;
    
    `

function Home() {

  const { t, i18n } = useTranslation();


  return (



    <div className="Homepage">
      <InformativeDiv>

        <h1>{t('main_title')}</h1>

      </InformativeDiv>

      <InformativeDiv>
        
          <p>{t('headline')}</p>
        

      </InformativeDiv>

      <Title>{t('services_copy')}</Title>

      <ServicesContainer />
      <Title>{t('skills_copy')}</Title>
      <SkillsContainer />
    </div>
  )

}










export default Home;
