import React from 'react';
import styled from 'styled-components';
import { useTranslation } from "react-i18next";

const WorkParent = styled.div`
    width:100%;
    display:flex;
    align-items:center;
    box-sizing:border-box;
    flex-wrap:wrap;
    border: 1px solid grey;
    margin:20px 0 ;
    padding:20px; 
    `;

const WorkThumbnail = styled.div`

    box-sizing:border-box;
    text-align:center;
    display:inline-block;
    width:40%;
    padding:0 15px;
    @media (max-width:768px){
      width:100%;
      padding-left:0;
    }
    & > img{
      width:100%;
    }
    vertical-align:middle;
`;


const SeeProject = styled.a`
    padding:10px;
    margin: 0 auto;
    margin-top:20px;
    font-weight:bold;
    background: #e1e1e1;
    border:0;
    color:#000000;
    &:hover{
      text-decoration:underline;

    }
    font-size:1.2rem;
        `
const WorkDescription = styled.div`
  width:60%;
  @media (max-width:768px){
    width:100%;
    padding-left:0;
  }
  box-sizing:border-box;
  padding-left:50px;
display:inline-block;
vertical-align:middle;
`

function WorkComponent(props) {

  const { t, i18n } = useTranslation();

  return (
    <WorkParent className="SkillComp">
      <WorkThumbnail className="WorkThumbnail">
        <h3>{props.title}</h3>
        <img src="https://bitbucket.org/jbgranja/web-portfolio/raw/2dbc16e3c9d3a641b430751c74359e4c1b057eb9/public/assets/website_placeholder.png" alt="" />
      </WorkThumbnail>
      <WorkDescription ><p>{props.description}</p><SeeProject  className='moreAbout' target='_blank' href={props.link}>

        {t('more_about_project')}
      </SeeProject></WorkDescription>

    </WorkParent>
  )
}

export default WorkComponent;
