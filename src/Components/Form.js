import React from 'react';
import styled from 'styled-components';

const FormLabel = styled.label`
    display:block;
`
const FormTextArea = styled.textarea`
    display:block;
    padding:5px;
`
const FormInput = styled.input`
    display:block;
    padding:5px;
`
const FormItem = styled.div`
    display:block;
    margin-bottom:20px;
`

const Selector = styled.select`
    display:block;
    padding:5px;
`



const Form = (props) => (
    <div>
        <form id="contact_form">
            <FormItem>
                <FormLabel>Email</FormLabel>
                <FormInput type="email"></FormInput>
            </FormItem>

            <FormItem>
                <FormLabel>Subject</FormLabel>
                <Selector>
                    <option>Ecommerce</option>
                    <option>Worpress Development</option>
                    <option>A/B Testing</option>
                    <option>Web Design</option>
                    <option>API/ RESTful API Development</option>
                    <option>Software Development</option>
                    <option>Other Service</option>
                </Selector>

            </FormItem>

            <FormItem>
                <FormLabel>Message</FormLabel>
                <FormTextArea cols="40" rows="6" type="text"></FormTextArea>

            </FormItem>

            <button value="submit" type="submit">Submit</button>
        </form>
    </div>
);
export default Form;
