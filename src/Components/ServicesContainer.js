import React from 'react';
import { useTranslation } from "react-i18next";
import { services } from '../Data.js';

import ServiceComponent from './ServiceComponent';

const data = require('../Data.js');
const services_data = data.services;

function ServicesContainer() {

    const { t, i18n } = useTranslation();
    const services = t('services',{ returnObjects: true });
    return (
        <div>
            {services_data.map(function (data, key) {
                let service = services[key];
                //console.log(service);
                return (
                    <ServiceComponent service={service} key={key} image={data['img']}></ServiceComponent>
                )
            })}
        </div>
    );
}

export default ServicesContainer;
