import React from 'react';
import styled from 'styled-components';
import { useTranslation } from "react-i18next";

const ServiceContainer = styled.div`
    vertical-align:middle;
    @media(max-width:768px){
      margin-top:20px;
      width:100%;
      
    }
    width:50%;
    display:inline-block;
`;

const Service = styled.div`
    box-sizing:border-box;
    text-align:center;
    margin:0 20px;
    padding:5px 0 20px 0;
    min-height:350px;
   
    `;

const Image = styled.img`
    max-width:400px;
    height:auto;
    margin:0 auto;
    border: 1px solid #cccccc;
    @media(max-width:425px){
     max-width:100%;
    } 
    `
function ServiceComponent (props)  {
  const { t, i18n } = useTranslation();

  //console.log(props.service.name)

  return(
    <ServiceContainer className="SkillComp">
    <Service>
      <h3>{props.service.name}</h3>

      <Image src={props.image} alt="" />
      <p>{props.service.description}</p>
    </Service>
  </ServiceContainer>

  )


}





export default ServiceComponent;
