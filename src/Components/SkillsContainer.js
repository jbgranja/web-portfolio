
import React, {
    Component
} from 'react';

import SkillComponent from './SkillComponent';

import styled from 'styled-components';

const data = require('../Data.js');

const skills_data = data.skills;

const Container = styled.div`
  text-align:center;
  margin-top:20px;
` 

class SkillsContainer extends Component {


    render() {

        var elements = [];
        for (let i = 0; i < skills_data.length; i++) {
            elements.push(<SkillComponent key = {i} topic={skills_data[i]['topic']} icon={skills_data[i]['icon']} level={skills_data[i]['level']}/>); 
        }
        
        return (
            <Container>{elements}</Container>
        );
    }
}

export default SkillsContainer;