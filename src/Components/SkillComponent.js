
import React, {
  Component
} from 'react';
import styled from 'styled-components';


const Skilldiv = styled.div`


@media (max-width: 768px) {
  flex-direction: column;
  width:33.333%;
}
width:25%;
    display:inline-block;
    margin:0 25px;
    box-sizing:border-box;
    position:relative;  
`;

const SkillLevel = styled.div`    
    margin:10px 0;
    height:10px;
    position:relative;
    `
// const BarLevel = styled.div`
//   background:#000000;
//   height:10px;
// `
class SkillComponent extends Component {

  render() {
    return (
      <Skilldiv>
        <div ><i className={this.props.icon}></i></div>
        <span>{this.props.topic}</span>
        <SkillLevel   topValue={this.props.level}>
        </SkillLevel>
      </Skilldiv>
    )
  }
}

export default SkillComponent;