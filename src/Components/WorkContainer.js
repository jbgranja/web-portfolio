import React from 'react';
import { useTranslation } from "react-i18next";

import WorkComponent from './WorkComponent';



function WorkContainer()  {   

    const { t, i18n } = useTranslation();
    const works = t('works', { returnObjects: true });

    var elements = [];
    for (let i = 0; i < works.length; i++) {
        elements.push(<WorkComponent key={i} description={works[i]['description']} link={works[i]['link']} title={works[i]['title']} />);
    }
    return (
        <div>
            {elements}
        </div>

    );

}

export default WorkContainer;
