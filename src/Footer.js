import React, { Component } from 'react';
import styled from 'styled-components';

const MainFooter = styled.div`
  text-align: center;
  padding:20px;
  background-color:#ccc;
  `
const Sicon = styled.a`
  color:#000000;
`

class Footer extends Component {

  render() {
    return (
        <MainFooter className="Footer" id="footer">
          <div className="Social_links">
            <Sicon target="_blank" rel="noreferrer noopener" href='https://github.com/juanxhoO'><i className="fab fa-github"></i></Sicon>
            <Sicon target="_blank" rel="noreferrer noopener" href='https://www.linkedin.com/in/jbgranja'><i className="fab fa-linkedin-in"></i></Sicon>
            <Sicon target="_blank" rel="noreferrer noopener" href='https://join.skype.com/invite/u3fP4tBwzzb9'><i className="fab fa-skype"></i></Sicon>
            <Sicon className="upwork" target="_blank" rel="noreferrer noopener" href='https://www.upwork.com/o/profiles/users/~01407431190ab7502d/'><img  alt="upwork logo" src="https://bitbucket.org/jbgranja/web-portfolio/raw/657ab28c53ec59582a6a55a04e252c5db13e4fc1/public/assets/upwork.png"/></Sicon>
          </div>

          <p>&copy; 2021 Juan Granja Full Stack Developer</p>

        </MainFooter>
    );
  }
}

export default Footer;
