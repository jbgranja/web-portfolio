import React, { Component } from 'react';
import './App.css';
import Header from './Header';
import styled from 'styled-components';
import Footer from './Footer';
import ContactFloater from './Components/ContactFloater';
import TawkMessengerReact from '@tawk.to/tawk-messenger-react';

const Container = styled.div`

padding:0;
    `;
function App() {
    
  return (
    <div className="App">
      <Container>
        <Header />
        <Footer />
        {/* <ContactFloater /> */}
        <TawkMessengerReact
                propertyId="652abdb6eb150b3fb9a15c2e"
                widgetId="1hcnfa5bk"/>
      </Container>
    </div>
  );
}
//new change

export default App;

