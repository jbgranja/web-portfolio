module.exports = {

    services: [{
        img: "https://bitbucket.org/jbgranja/web-portfolio/raw/9dde5a5fb34996762c0512c1d1d053688ede2770/public/assets/Ecommerce_dev.jpg"
    },
    {
        img: "https://bitbucket.org/jbgranja/web-portfolio/raw/9dde5a5fb34996762c0512c1d1d053688ede2770/public/assets/wordpress_dev.jpg"
    }, {

        img: "https://bitbucket.org/jbgranja/web-portfolio/raw/9dde5a5fb34996762c0512c1d1d053688ede2770/public/assets/ab-testing.jpg"
    }, {

        img: "https://bitbucket.org/jbgranja/web-portfolio/raw/9dde5a5fb34996762c0512c1d1d053688ede2770/public/assets/web-development.jpg"
    }, {

        img: "https://bitbucket.org/jbgranja/web-portfolio/raw/9dde5a5fb34996762c0512c1d1d053688ede2770/public/assets/API_dev.jpeg"
    },
    {
        img: "https://bitbucket.org/jbgranja/web-portfolio/raw/9dde5a5fb34996762c0512c1d1d053688ede2770/public/assets/Software_dev.jpg"
    }
    ],


    skills: [{
        topic: "PHP",
        icon: "fab fa-php",

    },
    {
        topic: "Javascript",
        icon: "fab fa-js",

    },
    {
        topic: "Java",
        icon: "fab fa-java",

    },
    {
        topic: "Node JS",
        icon: "fab fa-node",

    },
    {
        topic: "React JS",
        icon: "fab fa-react",

    }, {
        topic: "HTML 5",
        icon: "fab fa-html5",

    },
    {
        topic: "Sass",
        icon: "fab fa-sass",

    }, {
        topic: "Go",
        icon: "fab fa-golang",

    },
    {
        topic: "Wordpress",
        icon: "fab fa-wordpress",

    },
    {
        topic: "Google Tag Manager",
        icon: "far fa-file-code",

    },
    {
        topic: "Git",
        icon: "fa-brands fa-git",

    },

    {
        topic: "CSS",
        icon: "fa-brands fa-css3",

    }] 
}